-   HandlerMapping is an interface that **defines a mapping between requests and handler objects**.
-   While Spring MVC framework provides some ready-made implementations, the interface can be implemented by developers to provide customized mapping strategy.
-   Some of the implementations provided by Spring MVC namely
##  BeanNameUrlHandlerMapping
It is the **default HandlerMapping** implementation. BeanNameUrlHandlerMapping **maps request URLs to beans with the same name**.
##  SimpleUrlHandlerMapping
It is the most flexible HandlerMapping implementation. It allows for direct and declarative mapping between either bean instances and URLs or between bean names and URLs.