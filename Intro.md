-   Spring MVC is a Java framework which is used to build web applications. It follows the **Model-View-Controller** design pattern.
-   A Spring MVC provides an elegant solution to use MVC in spring framework by the help of DispatcherServlet. Here, DispatcherServlet is a class that receives the incoming request and maps it to the right resource such as controllers, models, and views.
-   ![](https://static.javatpoint.com/sppages/images/spring-web-model-view-controller.png)

-   **Model** - A model contains the data of the application. A data can be a single object or a collection of objects.
-   **Controller** - A controller contains the business logic of an application. Here, the @Controller annotation is used to mark the class as the controller.
-   **View** - A view represents the provided information in a particular format. Generally, JSP+JSTL is used to create a view page. Although spring also supports other view technologies such as Apache Velocity, Thymeleaf and FreeMarker.
-   **Front Controller** - In Spring Web MVC, the DispatcherServlet class works as the front controller. It is responsible to manage the flow of the Spring MVC application.

##  Flow of Spring Web MVC
![](https://static.javatpoint.com/sppages/images/flow-of-spring-web-mvc.png)
-   All the incoming request is intercepted by the DispatcherServlet that works as the front controller.
-   The DispatcherServlet gets an entry of handler mapping from the XML file and forwards the request to the controller.
-   The controller returns an object of ModelAndView.
-   The DispatcherServlet checks the entry of view resolver in the XML file and invokes the specified view component.
-----------------------
##  DispatcherServlet
The Spring Web model-view-controller (MVC) framework is designed around a DispatcherServlet that handles all the HTTP requests and responses.

-------------------
##  Controller
The **@Controller** annotation indicates that a particular class serves the role of a controller. The **@RequestMapping** annotation is used to map a URL to either an entire class or a particular handler method.

--------------------
##  Service
**@Service** annotation is used with classes that provide some business functionalities. Spring context will autodetect these classes when annotation-based configuration and classpath scanning is used.

-----------------
##  RequestParam
-   The **@RequestParam** annotation is used to read the form data and **bind it automatically to the parameter** present in the provided method. So, it **ignores the requirement of HttpServletRequest** object to read the provided data.
-   Including form data, it also maps the request parameter to query parameter and parts in multipart requests.
-------------------
##  @GetMapping
-   It is specialized version of ```@RequestMapping``` annotation that acts as a shortcut for ```@RequestMapping(method = RequestMethod.GET)```.
-   @GetMapping annotated methods handle the HTTP GET requests matched with given URI expression.
##  @PostMapping
-   It is specialized version of ```@RequestMapping``` annotation that acts as a shortcut for ```@RequestMapping(method = RequestMethod.POST)```.
-   @PostMapping annotated methods handle the HTTP POST requests matched with given URI expression.
-----------------------
# Exception Handling
-   It is very important to make sure you are not sending server exceptions to client.
-   Spring MVC Framework provides following ways to help us achieving robust exception handling.
##  Controller Based
-   We can define exception handler methods in our controller classes. All we need is to annotate these methods with **@ExceptionHandler** annotation. This annotation takes Exception class as argument.
-    So if we have defined one of these for Exception class, then **all the exceptions thrown by our request handler method will have handled**.
-   These exception handler methods are just like other request handler methods and we can build error response and respond with different error page.
-   If there are multiple exception handler methods defined, then **handler method that is closest to the Exception class is used.**
##  Global Exception Handler
-   Exception Handling is a cross-cutting concern, it should be done for all the pointcuts in our application. 
-   That’s why Spring provides **@ControllerAdvice** annotation that we can use with any class to define our global exception ha-   Spring Framework provides HandlerExceptionResolver interface that we can implement to create global exception handler.
-   The reason behind this additional way to define global exception handler is that Spring framework also provides default implementation classes that we can define in our spring bean configuration file to get spring framework exception handling benefits.ndler.
-   The **handler methods in Global Controller Advice is same as Controller based exception handler methods** and used when controller class is not able to handle the exception.

![](https://www.programmersought.com/images/510/2e8eaf48a37ede7fa2b3f72c4a9d8e66.JPEG)